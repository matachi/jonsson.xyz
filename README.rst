=======
danj.io
=======

| Site URL: `<http://danj.io>`_
| Source code: `<https://gitlab.com/matachi/danj.io>`_

Setup
=====

Run::

    $ sudo dnf install golang hugo python2-docutils python3-docutils

Develop
=======

Run::

    $ hugo serve

Build
=====

Run::

    $ hugo
