+++
title = "Notes"
summary = "HTML5 single-page application built with AngularJS."
weight = -8
slug = "notes"
when = "Mid 2013"
src = "https://github.com/matachi/html5-notes-app"
platforms = ["Web browser"]
languages = ["JavaScript"]
libraries = ["AngularJS", "Bootstrap"]
cover = "/images/projects/covers/notes.jpg"
screenshots = [
  "/images/projects/screenshots/notes-1.png",
  "/images/projects/screenshots/notes-2.png",
]

[[links]]
title = "Notes app"
url = "http://matachi.github.io/html5-notes-app/"
+++

{{< project >}}
