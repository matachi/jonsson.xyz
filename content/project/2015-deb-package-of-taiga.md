+++
title = ".deb package of Taiga"
summary = "During a course called \"software evolution project\" I put together an experimental .deb package that would install Taiga on Ubuntu."
weight = -24
slug = "deb-package-of-taiga"
when = "Autumn 2015"
src = "https://github.com/matachi/taiga-debian-package"
platforms = ["Desktop"]
tools = ["dpkg", "apt", "VirtualBox"]
cover = "/images/projects/covers/taiga-deb.jpg"
+++

{{< project >}}
