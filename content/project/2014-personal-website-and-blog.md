+++
title = "Personal website and blog"
summary = "A previous website."
weight = -21
slug = "personal-website-and-blog"
when = "Autumn 2014"
src = "https://github.com/matachi/hydra"
platforms = ["Web browser"]
languages = ["Python", "JavaScript"]
libraries = ["Django", "Bootstrap"]
tools = ["gulp", "Docker", "virtualenv", "nginx", "uWSGI"]
cover = "/images/projects/covers/personal-website-and-blog.jpg"
screenshots = [
  "/images/projects/screenshots/hydra-1.png",
]
+++

{{< project >}}
