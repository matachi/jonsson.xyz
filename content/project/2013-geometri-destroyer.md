+++
title = "Geometri Destroyer"
summary = "Each level has a set of geometrical objects and your goal is to remove a number of the green ones without letting the blue ones touch the ground."
weight = -7
slug = "geometri-destroyer"
when = "Early 2013"
src = "https://github.com/matachi/geometri-destroyer"
platforms = ["Desktop computer", "Android"]
languages = ["Java"]
libraries = ["libGDX", "Box2d"]
cover = "/images/projects/covers/geometri-destroyer.jpg"
screenshots = [
  "/images/projects/screenshots/geometri-destroyer-1.png",
  "/images/projects/screenshots/geometri-destroyer-2.png",
  "/images/projects/screenshots/geometri-destroyer-3.png",
  "/images/projects/screenshots/geometri-destroyer-4.png",
]

[[links]]
title = "Download JAR for your computer"
url = "https://files.matachi.se/owncloud/index.php/s/4XplsfBtR7eCav5"

[[links]]
title = "Google Play"
url = "https://play.google.com/store/apps/details?id=se.danielj.geometridestroyer"
+++

{{< project >}}
