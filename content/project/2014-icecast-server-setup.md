+++
title = "Icecast server setup"
summary = "An experiment to run an Icecast server that streams music in Opus (a more efficient audio format than MP3). Liquidsoap in one end reads a playlist and plays its songs, and on the other end it's connected to Icecast at a mountpoint. The user then connects to Icecast and recieves an Opus stream. To make this setup easy to install and replicate it's done in a Docker container."
weight = -15
slug = "icecast-server-setup"
when = "Early 2014"
src = "https://github.com/matachi/icecast-server-setup"
platforms = ["Desktop computer"]
libraries = ["Flask"]
tools = ["Docker", "Liquidsoap", "Icecast"]
cover = "/images/projects/covers/icecast-server-setup.jpg"
screenshots = [
  "/images/projects/screenshots/icecast-server-setup-1.png",
  "/images/projects/screenshots/icecast-server-setup-2.png",
  "/images/projects/screenshots/icecast-server-setup-3.png",
]
+++

{{< project >}}
