+++
title = "SomaFM Firefox OS app"
summary = "A SomaFM HTML5 app for Firefox OS."
weight = -18
slug = "somafm-firefox-os-app"
when = "Summer 2014"
src = "https://github.com/matachi/somafm-firefox-os-app"
platforms = ["Firefox OS"]
languages = ["JavaScript"]
libraries = ["Underscore.js", "Backbone.js", "jQuery", "Building Blocks"]
tools = ["Grunt"]
cover = "/images/projects/covers/somafm-firefox-os-app.jpg"
screenshots = [
  "/images/projects/screenshots/somafm-firefox-os-1.png",
  "/images/projects/screenshots/somafm-firefox-os-2.png",
  "/images/projects/screenshots/somafm-firefox-os-3.png",
]

[[links]]
title =  "Firefox Marketplace"
url = "https://marketplace.firefox.com/app/somafm"
+++

{{< project >}}
