+++
title = "Back up a website"
summary = "Script to download a site whose content is hosted on a shared host. This is useful if PHP, MySQL and FTP are supported, which they often are on a shared host, but not the ability to run any Linux commands. One component of the script is a PHP API that's installed on the host. The other comonent is a Python script that's run from the command-line."
weight = -16
slug = "back-up-a-website"
when = "Early 2014"
src = "https://github.com/matachi/backup-site"
platforms = ["Desktop computer"]
languages = ["Python", "PHP"]
tools = ["Docker"]
cover = "/images/projects/covers/backup-website.jpg"
+++

{{< project >}}
