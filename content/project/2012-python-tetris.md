+++
title = "Python tetris"
summary = "This prototype I wrote in 2012 and isn't finished."
weight = -4
slug = "python-tetris"
when = "Late 2012"
src = "https://github.com/matachi/python-tetris"
platforms = ["Desktop computer"]
languages = ["Python"]
libraries = ["Pygame"]
cover = "/images/projects/covers/python-tetris.jpg"
screenshots = [
  "/images/projects/screenshots/python-tetris-1.png",
  "/images/projects/screenshots/python-tetris-2.png",
  "/images/projects/screenshots/python-tetris-3.png",
  "/images/projects/screenshots/python-tetris-4.png",
]
+++

{{< project >}}
