+++
title = "Steam survey stats"
summary = "A small program that I ran for a while during 2013. It pulls down the Steam Hardware & Software Survey page with this month's OS statistics and greps the stats with the help of Beautiful Soup. These numbers are automatically stored in a SQLite database. In a separate branch is a set of HTML pages that present these stats with D3."
weight = -13
slug = "steam-survey-stats"
when = "2013"
src = "https://github.com/matachi/steam-survey-stats"
platforms = ["Web browser", "Desktop computer"]
languages = ["JavaScript", "Python"]
libraries = ["D3.js", "Beautiful Soup"]
cover = "/images/projects/covers/steam-survey-stats.jpg"
screenshots = [
  "/images/projects/screenshots/steam-survey-stats-1.png",
  "/images/projects/screenshots/steam-survey-stats-2.png",
  "/images/projects/screenshots/steam-survey-stats-3.png",
  "/images/projects/screenshots/steam-survey-stats-4.png",
]

[[links]]
title = "Steam survey charts"
url = "http://matachi.github.io/steam-survey-stats/"
+++

{{< project >}}
