+++
title = "Wooden Labyrinth"
summary = "Wooden labyrinth game in HTML5."
weight = -20
slug = "wooden-labyrinth"
when = "Summer 2014"
src = "https://github.com/matachi/wooden-labyrinth-game"
platforms = ["Web browser"]
languages = ["JavaScript"]
libraries = ["Lo-Dash", "three.js", "RequireJS", "box2d.js"]
tools = ["gulp", "JSDoc"]
cover = "/images/projects/covers/wooden-labyrinth.jpg"
screenshots = [
  "/images/projects/screenshots/wooden-labyrinth-1.png",
  "/images/projects/screenshots/wooden-labyrinth-2.png",
  "/images/projects/screenshots/wooden-labyrinth-3.png",
]

[[links]]
title =  "Play the game"
url = "http://matachi.github.io/wooden-labyrinth-game/"

[[links]]
title =  "JSDoc"
url = "http://matachi.github.io/wooden-labyrinth-game/doc/"
+++

{{< project >}}
