+++
title = "Advent of Code 2015 solutions"
summary = "Did Advent of Code's programming challenges."
weight = -28
slug = "advent-of-code-2015-solutions"
when = "Summer 2016"
src = "https://gitlab.com/matachi/advent-of-code-2015"
languages = ["Python"]
cover = "/images/projects/covers/advent-of-code-2015.jpg"
+++

{{< project >}}
