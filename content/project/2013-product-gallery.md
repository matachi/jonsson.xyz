+++
title = "Product gallery"
summary = "HTML5 single-page AngularJS application with a REST backend implemented in Django."
weight = -11
slug = "product-gallery"
when = "Autumn 2013"
src = "https://github.com/matachi/product-gallery"
platforms = ["Web browser"]
languages = ["JavaScript", "Python", "Shell"]
libraries = ["AngularJS", "Django", "Django REST Framework", "Bootstrap"]
tools = ["virtualenv"]
cover = "/images/projects/covers/product-gallery.jpg"
screenshots = [
  "/images/projects/screenshots/product-gallery-1.png",
  "/images/projects/screenshots/product-gallery-2.png",
]

[[links]]
title = "YouTube video showcasing the app"
url = "https://www.youtube.com/watch?v=hf9UK7WmRD0"
+++

{{< project >}}
