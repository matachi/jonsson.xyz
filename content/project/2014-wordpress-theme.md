+++
title = "WordPress theme"
summary = "WordPress theme that I made for my sister's blog."
weight = -19
slug = "wordpress-theme"
when = "Summer 2014"
src = "https://github.com/matachi/hesitant"
platforms = ["WordPress"]
languages = ["PHP", "JavaScript"]
libraries = ["Bootstrap"]
tools = ["Grunt", "Docker"]
cover = "/images/projects/covers/hesitant.jpg"
screenshots = [
  "/images/projects/screenshots/hesitant-1.png",
]

[[links]]
title =  "My sister's blog"
url = "http://misslisibell.se/"
+++

{{< project >}}
