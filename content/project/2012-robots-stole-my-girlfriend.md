+++
title = "Robots Stole My Girlfriend"
summary = "The result of a programming project course at Chalmers, done together with Johan Grönvall and Johan Rignäs."
weight = -2
slug = "robots-stole-my-girlfriend"
when = "Spring 2012"
src = "https://github.com/matachi/Robots-Stole-My-Girlfriend"
platforms = ["Desktop computer"]
languages = ["Java"]
libraries = ["Slick2D"]
cover = "/images/projects/covers/robots-stole-my-girlfriend.jpg"
screenshots = [
  "/images/projects/screenshots/rsmg-1.jpg",
  "/images/projects/screenshots/rsmg-2.jpg",
  "/images/projects/screenshots/rsmg-3.jpg",
  "/images/projects/screenshots/rsmg-4.jpg",
  "/images/projects/screenshots/rsmg-5.jpg",
  "/images/projects/screenshots/rsmg-6.jpg"]
+++

{{< project >}}
