+++
title = "Skuttande Nyan Cat"
summary = "This is a \"run and jump\" game for Android. It's built with the game library libGDX and the entity component system library Artemis. It was previously available on Google Play, but it received a DMCA takedown request from the creator of Nyan Cat and got pulled down."
weight = -6
slug = "skuttande-nyan-cat"
when = "2012 / 2013"
src = "https://github.com/matachi/skuttande-nyan-cat"
platforms = ["Desktop computer", "Android"]
languages = ["Java"]
libraries = ["libGDX", "Artemis"]
cover = "/images/projects/covers/skuttande-nyan-cat.jpg"
screenshots = [
  "/images/projects/screenshots/skuttande-nyan-cat-1.png",
  "/images/projects/screenshots/skuttande-nyan-cat-2.png",
  "/images/projects/screenshots/skuttande-nyan-cat-3.png",
]

[[links]]
title = "Download JAR for your computer"
url = "https://files.matachi.se/owncloud/index.php/s/Dy9YBjKVmHKDqth"
+++

{{< project >}}
