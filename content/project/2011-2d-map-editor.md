+++
title = "2D map editor"
summary = "2D map editor used to build the maps in Robots Stole My Girlfriend."
weight = -1
slug = "2d-map-editor"
when = "2011 / 2012"
src = "https://github.com/matachi/2D-Map-Editor"
platforms = ["Desktop computer"]
languages = ["Java"]
libraries = ["Swing"]
cover = "/images/projects/covers/2d-map-editor.jpg"
screenshots = [
  "/images/projects/screenshots/map-editor-1.jpg",
  "/images/projects/screenshots/map-editor-2.jpg",
  "/images/projects/screenshots/map-editor-3.jpg"
]

[[links]]
title = "Video showcasing the application"
url = "https://github.com/matachi/2D-Map-Editor"
+++

{{< project >}}
