+++
title = "Smart lagerhållningssystem med RFID"
summary = "Bachelor project done together with Philip Dahlstedt, Fredrik Ek, Gabriel Andersson, Marc Jamot and Martin Ljungdahl."
weight = -17
slug = "smart-lagerhallningssystem-med-rfid.md"
when = "Spring 2014"
platforms = ["Desktop computer"]
languages = ["Java", "Python", "JavaScript", "C"]
libraries = ["Play Framework", "Tkinter"]
cover = "/images/projects/covers/smart-lagerhallningssystem-med-rfid.jpg"

[[links]]
title = "Read the report"
url = "http://studentarbeten.chalmers.se/publication/203267-smart-lagerhallningssystem-med-rfid"
+++

{{< project >}}
