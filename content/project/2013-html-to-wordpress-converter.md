+++
title = "HTML to WordPress converter"
summary = "Python script with a web based front-end to convert static HTML pages to WordPress pages. Used this script to convert pages on <a href=\"http://andersj.se/index_gammal.html\">http://andersj.se/index_gammal.html</a> over to the WordPress site at <a href=\"http://andersj.se/\">http://andersj.se/</a>."
weight = -12
slug = "html-to-wordpress-converter"
when = "Autumn 2013"
src = "https://github.com/matachi/html-to-wordpress"
platforms = ["Web browser"]
languages = ["Python"]
libraries = ["Beautiful Soup", "Flask", "Bootstrap"]
cover = "/images/projects/covers/html-to-wordpress-converter.jpg"
+++

{{< project >}}
