+++
title = "Master's thesis"
summary = "Master's thesis project with the title \"A Case Study of Interactive Conflict-Resolution Support in Software Configuration.\" I implemented a prototype, based on the Linux kernel's configuration tool xconfig, which was able to automatically resolve unmet dependencies."
weight = -27
slug = "masters-thesis"
when = "Spring 2016"
languages = ["C", "Scala"]
libraries = ["GLib", "Check"]
tools = ["Valgrind", "GNU make", "Kcofig"]
cover = "/images/projects/covers/master-thesis.jpg"

[[links]]
title = "Link to the thesis"
url = "http://studentarbeten.chalmers.se/publication/238168-a-case-study-of-interactive-conflict-resolution-support-in-software-configuration"
+++

{{< project >}}
