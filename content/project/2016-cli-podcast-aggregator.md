+++
title = "CLI podcast aggregator"
summary = "A podcast aggregator which stores its state as YAML and CSV files, which makes it easy to version control the download history."
weight = -25
slug = "cli-podcast-aggregator"
when = "2015 / 2016"
src = "https://github.com/matachi/riley"
platforms = ["Desktop"]
languages = ["Python"]
libraries = ["feedparser", "PyYAML", "requests"]
cover = "/images/projects/covers/riley.jpg"
+++

{{< project >}}
