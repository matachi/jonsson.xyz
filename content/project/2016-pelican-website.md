+++
title = "Pelican website"
summary = "This website was previously made with the static site generator Pelican, and hosted and built on GitLab. (Now it's instead using Hugo.)"
weight = -26
slug = "pelican-website"
when = "Spring 2016"
src = "https://gitlab.com/matachi/danj.io/"
platforms = ["Web browser"]
languages = ["Python", "HTML", "CSS"]
libraries = ["Pelican"]
tools = ["GitLab Continuous Integration", "GNU make"]
cover = "/images/projects/covers/danjio.jpg"
+++

{{< project >}}
