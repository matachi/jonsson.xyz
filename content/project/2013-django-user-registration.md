+++
title = "Django user registration"
summary = "An experiment with Django Userena, which is a module that makes it easy to register user accounts on a Django site. The site also has functionality that verifies user form input asynchronously by communicating with the server through an API built with Django REST Framework."
weight = -10
slug = "django-user-registration"
when = "2013"
src = "https://github.com/matachi/django-user-registration-demo"
platforms = ["Web browser"]
languages = ["JavaScript", "Python", "Shell"]
libraries = ["Django", "Django REST Framework", "Bootstrap", "Django Userena"]
tools = ["virtualenv"]
cover = "/images/projects/covers/django-user-registration.jpg"
screenshots = [
  "/images/projects/screenshots/django-user-registration-1.png",
  "/images/projects/screenshots/django-user-registration-2.png",
  "/images/projects/screenshots/django-user-registration-3.png",
]

[[links]]
title = "YouTube video showcasing the app"
url = "https://www.youtube.com/watch?v=k9eoC6APhHM"
+++

{{< project >}}
