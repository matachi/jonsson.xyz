+++
title = "C++ tetris"
summary = "Tetris written in C++ with the help of the library SDL. The graphics is done with OpenGL and the compilation by CMake."
weight = -5
slug = "cpp-tetris"
when = "Early 2013"
src = "https://github.com/matachi/tetris-cpp"
platforms = ["Desktop computer"]
languages = ["C++"]
libraries = ["OpenGL", "SDL"]
tools = ["CMake"]
cover = "/images/projects/covers/c-tetris.jpg"
screenshots = [
  "/images/projects/screenshots/c-tetris-1.png",
  "/images/projects/screenshots/c-tetris-2.png",
  "/images/projects/screenshots/c-tetris-3.png",
  "/images/projects/screenshots/c-tetris-4.png",
]

[[links]]
title = "YouTube video showcasing the game"
url = "https://www.youtube.com/watch?v=bR8GbbcmP_Y"
+++

{{< project >}}
