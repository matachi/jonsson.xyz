+++
title = "YouTube thumbnails WordPress widget"
summary = "WordPress widget showing video thumbnails of recent videos from a YouTube channel or playlist. It is a fork of another plugin, where I stripped out code and libraries I felt made it unnecessarily heavy."
weight = -22
slug = "youtube-thumbnails-wordpress-widget"
when = "Winter 2014 / 2015"
src = "https://github.com/matachi/lightweight-youtube-channel-widget"
platforms = ["WordPress"]
languages = ["PHP"]
tools = ["Docker", "Subversion"]
cover = "/images/projects/covers/lightweight-youtube-channel-widget.jpg"
screenshots = [
  "/images/projects/screenshots/lycw-1.png",
  "/images/projects/screenshots/lycw-2.png",
  "/images/projects/screenshots/lycw-3.png",
  "/images/projects/screenshots/lycw-4.png",
]

[[links]]
title = "Download and use the plugin on your WordPress"
url = "https://wordpress.org/plugins/lightweight-youtube-channel-widget/"
+++

{{< project >}}
