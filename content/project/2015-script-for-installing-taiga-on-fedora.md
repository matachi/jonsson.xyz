+++
title = "Script for installing Taiga on Fedora"
summary = "During a course called \"software evolution project\" I made a Bash script that installs Taiga on Fedora."
weight = -23
slug = "script-for-installing-taiga-on-fedora"
when = "Autumn 2015"
src = "https://github.com/matachi/taiga-scripts/tree/fedora"
platforms = ["Desktop"]
languages = ["Bash"]
tools = ["VirtualBox"]
cover = "/images/projects/covers/taiga-fedora.jpg"
+++

{{< project >}}
