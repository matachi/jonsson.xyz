+++
title = "Stopwatch"
summary = "This stopwatch is implemented in JavaScript with AngularJS."
weight = -9
slug = "stopwatch"
when = "Mid 2013"
src = "https://github.com/matachi/stopwatch"
platforms = ["Web browser"]
languages = ["JavaScript"]
libraries = ["AngularJS"]
cover = "/images/projects/covers/stopwatch.jpg"
screenshots = [
  "/images/projects/screenshots/stopwatch-1.png",
  "/images/projects/screenshots/stopwatch-2.png",
  "/images/projects/screenshots/stopwatch-3.png",
  "/images/projects/screenshots/stopwatch-4.png",
  "/images/projects/screenshots/stopwatch-5.png",
]

[[links]]
title = "Stopwatch"
url = "http://matachi.github.io/stopwatch/"
+++

{{< project >}}
