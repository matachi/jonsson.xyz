+++
title = "Ultra Extreme"
summary = "Android game built together with Björn Persson Mattsson, Johan Grönvall and Viktor Anderling during a Chalmers project course."
weight = -3
slug = "ultra-extreme"
when = "Autumn 2012"
src = "https://github.com/DAT255-group20/UltraExtreme"
platforms = ["Android"]
languages = ["Java"]
libraries = ["AndEngine"]
cover = "/images/projects/covers/ultra-extreme.jpg"
screenshots = [
  "/images/projects/screenshots/ultra-extreme-1.png",
  "/images/projects/screenshots/ultra-extreme-2.png",
  "/images/projects/screenshots/ultra-extreme-3.png",
  "/images/projects/screenshots/ultra-extreme-4.png",
  "/images/projects/screenshots/ultra-extreme-5.png",
  "/images/projects/screenshots/ultra-extreme-6.png",
  "/images/projects/screenshots/ultra-extreme-7.png",
  "/images/projects/screenshots/ultra-extreme-8.png",
  "/images/projects/screenshots/ultra-extreme-9.png",
  "/images/projects/screenshots/ultra-extreme-10.png"]

[[links]]
title = "Android APK"
url = "https://github.com/DAT255-group20/UltraExtreme/blob/develop/doc/UltraExtreme.apk"
+++

{{< project >}}
