+++
title = "Podcast aggregator"
summary = "Web based podcast aggregator built in Python with Django and a bunch of other libraries. On the server-side it continuously fetches new episodes from the podcasts' feeds and adds them to the website."
weight = -14
slug = "podcast-aggregator"
when = "Winter 2013 / 2014"
src = "https://github.com/matachi/sputnik"
platforms = ["Web browser"]
languages = ["Python", "JavaScript"]
libraries = ["Django", "Django REST Framework", "Beautiful Soup", "Bootstrap",
  "django-allauth", "Haystack", "feedparser"]
tools = ["Docker", "virtualenv", "nginx", "uWSGI"]
cover = "/images/projects/covers/podflare-podcast-aggregator.jpg"
screenshots = [
  "/images/projects/screenshots/podflare-1.png",
  "/images/projects/screenshots/podflare-2.png",
  "/images/projects/screenshots/podflare-3.png",
  "/images/projects/screenshots/podflare-4.png",
  "/images/projects/screenshots/podflare-5.png",
  "/images/projects/screenshots/podflare-6.png",
  "/images/projects/screenshots/podflare-7.png",
  "/images/projects/screenshots/podflare-8.png",
  "/images/projects/screenshots/podflare-9.png",
  "/images/projects/screenshots/podflare-10.png",
]
+++

{{< project >}}
