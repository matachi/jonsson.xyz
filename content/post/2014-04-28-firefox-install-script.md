+++
title = "Script to install and update Firefox Beta, Aurora or Nightly on Linux"
slug = "script-to-install-and-update-firefox-beta-aurora-or-nightly-on-linux"
date = "2014-04-28T00:00:00Z"
+++

If Beta, Aurora or Nightly isn't packaged for your distribution you can use the following script to automatically download the compiled version from Mozilla's FTP. The script will download Aurora for x86, but it can easily be modified to download Nightly or Beta instead. Nightly can be found on <https://ftp.mozilla.org/pub/mozilla.org/firefox/nightly/latest-trunk/> and Beta can be found on <https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/latest-beta/linux-x86_64/en-US/>. I'm myself using the script to update Firefox Aurora on Fedora.

Script:

    #!/bin/bash
    cd /opt/
    FILE=`curl https://ftp.mozilla.org/pub/mozilla.org/firefox/nightly/latest-mozilla-aurora/ |
        grep 'firefox-.\{2,6\}\.en-US\.linux-x86_64\.tar\.bz2' -o -m 1 |
        head -1`
    wget -O /tmp/aurora.tar.bz2 \
        https://ftp.mozilla.org/pub/mozilla.org/firefox/nightly/latest-mozilla-aurora/$FILE
    mkdir -p /tmp/aurora
    tar -C /tmp/aurora -xjf /tmp/aurora.tar.bz2
    rm -r /opt/aurora
    mv /tmp/aurora/firefox /opt/aurora

Save it in a file named `update-aurora.sh` in your home directory and add execution permission:

~~~bash
$ chmod u+x update-aurora.sh
~~~

Then run the script as root:

~~~bash
$ sudo ./update-aurora.sh
~~~

Make the executable available as a command via the CLI:

~~~bash
$ sudo ln -s /opt/aurora/firefox /usr/local/bin/aurora
~~~

To launch the browser on Gnome 3; press ALT+F2, type `aurora` and press enter.

If you want to stop the browser's nagging about searching for a new version:

1. Open `about:config` in Firefox.
2. Set both `app.update.auto` and `app.update.enabled` to false.
