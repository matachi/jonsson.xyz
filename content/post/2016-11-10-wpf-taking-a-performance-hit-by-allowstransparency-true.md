+++
title = "WPF taking a performance hit by AllowsTransparency=\"True\""
tags = ["C#", "Microsoft", "Windows", "WPF"]
date = "2016-11-10T21:15:00+01:00"
slug = "wpf-taking-performance-hit-allowstransparencytrue"
+++

![Window corner](/images/corner.jpg)

A quite complex C# WPF project I'm working on is riddled with various
performance issues. After shaving off component after component, trying to
locate why everything in the application felt sluggish, I arrived at
`AllowsTransparency="True"` and `Background="Transparent`. The offending code
looked like this:

~~~xml
<Window x:Class="MainWindow"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    ...
    WindowStyle="None"
    AllowsTransparency="True"
    Background="Transparent"
    WindowState="Normal"
    x:Name="mainWindow">
~~~

The window's background is transparent to be able to create custom rounded
corners (see the above picture with and without the effect). Maybe fancy in
somebody's mind, but apparently it makes the window render in software rather
than hardware, which explains why it slows to a crawl. I'm not convinced that
prettier window corners is worth a significant performance penalty.

Read more on:

* [http://stackoverflow.com/a/36258961/595990](http://stackoverflow.com/a/36258961/595990)
* [http://stackoverflow.com/q/21576016/595990](http://stackoverflow.com/q/21576016/595990)
* [https://social.msdn.microsoft.com/Forums/vstudio/en-US/c91e7d8e-aa59-4b44-982d-e204783d12b4/allowstransparencytrue-is-main-reason-of-wpf-performance-?forum=wpf](https://social.msdn.microsoft.com/Forums/vstudio/en-US/c91e7d8e-aa59-4b44-982d-e204783d12b4/allowstransparencytrue-is-main-reason-of-wpf-performance-?forum=wpf)
* [https://msdn.microsoft.com/en-us/library/system.windows.controls.control.background(v=vs.110).aspx](https://msdn.microsoft.com/en-us/library/system.windows.controls.control.background(v=vs.110).aspx)
* [https://msdn.microsoft.com/en-us/library/system.windows.window.allowstransparency(v=vs.110).aspx](https://msdn.microsoft.com/en-us/library/system.windows.window.allowstransparency(v=vs.110).aspx)

