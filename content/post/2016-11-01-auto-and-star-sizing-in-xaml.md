+++
title = "Auto and star (*) sizing in XAML"
tags = ["Microsoft", "Windows", "WPF", "XAML"]
date = "2016-11-01T21:17:00+01:00"
slug = "auto-and-star-sizing-in-xaml"
+++

![Visual Studio 2015](/images/visual-studio-2015.jpg)

In XAML there are three ways to specify widths and heights. Except for
specifying the number of pixels as a double number, there are also the keywords
**Auto** and **\*** (an asterisk). These three options work as follows:

* *Double value*. A floating-point number or integer that specifies the width
  or height in number of pixels.
* *Auto*. The keyword `Auto` sets the width or height of the column or row to
  that of its content.
* *Star*. The keyword `*` makes the column or row expand to fill the remaining
  space within the container. If the star is preceded with a number, it's
  interpreted as a factor.

Looking at the following code example, the grid's width is set to 600 pixels.
Within the grid, there are three columns. The first column's width is set 3 /
(3 + 2 + 1) = ½ of the available space, which in this case is equal to 300
pixels. Analogously, the second column's width is computed to 200 pixels and
the third to 100 pixels.

~~~xml
<Grid Width="600">
    <Grid.ColumnDefinitions>
        <ColumnDefinition Width="3*" />
        <ColumnDefinition Width="2*" />
        <ColumnDefinition Width="1*" />
    </Grid.ColumnDefinitions>
    <Button>Button 1</Button>
    <Button Grid.Column="1">Button 2</Button>
    <Button Grid.Column="2">Button 3</Button>
</Grid>
~~~

Continuing with the above example with a 600 pixels wide grid containing three
columns, here are three examples of their computed widths:

<table>
    <tbody>
        <tr>
            <th>Column 0</th>
            <th>Column 1</th>
            <th>Column 2</th>
            <th>Result</th>
        </tr>
        <tr>
            <td>200</td>
            <td>100</td>
            <td>*</td>
            <td>200,100,400</td>
        </tr>
        <tr>
            <td>200</td>
            <td>*</td>
            <td>*</td>
            <td>200,250,250</td>
        </tr>
        <tr>
            <td>200</td>
            <td>*</td>
            <td>4*</td>
            <td>200,100,400</td>
        </tr>
    </tbody>
</table>

Read more about sizing in XAML:

* [msdn.microsoft.com/en-us/library/windows/apps/xaml/windows.ui.xaml.gridlength.aspx](https://msdn.microsoft.com/en-us/library/windows/apps/xaml/windows.ui.xaml.gridlength.aspx)

