+++
title = "Optional, nil, wrapping and unwrapping in Swift"
tags = ["Apple", "Swift"]
date = "2016-11-02T21:18:00+01:00"
slug = "optional-nil-wrapping-and-unwrappind-in-swift"
+++

![Xcode](/images/xcode-swift.jpg)

To express the possible absence of a variable's value in Swift, we need to use
optionals. A regular variable is declared in the following way:

~~~swift
var i: Int = 123
~~~

While an optional `Int` is declared like this:

~~~swift
var j: Int? = 123
~~~

What we are able to do with the optional `Int`, as opposed to the regular
`Int`, is to assign `nil` to it:

~~~swift
j = nil
~~~

Here, `nil` expresses the absence of a value.

A simple example of the usefulness of `nil` can be seen here:

~~~swift
var possibleNumber = "123"
var convertedNumber = Int(possibleNumber)
~~~

`possibleNumber` is inferred by Swift to be a `String`, while `convertedNumber`
is inferred to be an optional `Int`. It becomes an optional `Int` due to the
constructor's signature. It returns `nil` when the supplied `String` cannot
successfully be converted to an `Int`.

We can then use an if statement to find out if the variable contains a value or
not:

~~~swift
if convertedNumber != nil {
    print("My number is \(convertedNumber).")
}
~~~

However, the above piece of code will print:

    My number is Optional(123).

When we are sure that the variable contains a value, we may force unwrap
its value by appending an exclamation mark to where the variable is being
used:

~~~swift
if convertedNumber != nil {
    print("My number is \(convertedNumber!).")
}
~~~

The value in `convertedNumber` will now be unwrapped, and the following string
will be printed instead:

    My number is 123.

Returning to the declaration of an optional `Int`:

~~~swift
var k: Int? = 123
~~~

Here, `Int?` is a short form of `Optional<Int>`. These two are therefore
equivalent:

~~~swift
var k: Int? = 123
var k: Optional<Int> = 12
~~~

To explicitly wrap a value, use the function `Optional.some(...)`:

~~~swift
k = Optional.some(42)
~~~

Note though that by simply assigning an `Int` to `k` will implicitly wrap it.

Lastly, another way to assign `nil` to `k` is by assigning `Optional.none` to
it:

~~~swift
k = Optional.none
~~~

Read more about optionals, nil, wrapping, unwrapping, etc, on:

* [developer.apple.com/reference/swift/optional](https://developer.apple.com/reference/swift/optional)
* [developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html)

