+++
title = "Moving from CyanogenMod to Lineage OS"
tags = ["Android", "LineageOS"]
date = "2017-01-29T12:00:00+01:00"
slug = "moving-from-cyanogenmod-to-lineageos"
+++

I own a [OnePlus One](https://en.wikipedia.org/wiki/OnePlus_One) Android phone,
and have been using at as my primary phone for the last two years. Immediately
when I got it, I rooted it and put CyanogenMod on it. I have been a loyal
CyanogenMod user for a very long time, including previous phones. However, a
few weeks ago, CyanogenMod reached a bitter end when Cyanogen Inc. decided to
[shut down the
project](https://arstechnica.com/information-technology/2016/12/cyanogen-inc-shuts-down-cyanogenmod-in-christmas-bloodbath/).
To replace it, a community effort called [Lineage OS](http://lineageos.org/)
has started, and they have already released nightly builds for many phones. I
find this reassuring, since OnePlus seem to have abandoned the OnePlus One,
considering that the last version of [their own
ROM](http://downloads.oneplus.net/) for the model was released a whole year
ago, in January 19, 2016.

I was running CyanogenMod 13 (based on Android 6) and went to Lineage OS 14.1
(based on Android 7.1). It felt best to do a complete wipe of the phone and
start anew, which is what I did. However, I did use the app
[oandbackup](https://f-droid.org/repository/browse/?fdfilter=backup&fdid=dk.jens.backup)
to backup all apps I had installed myself, and then restore them on Lineage.
This was definitely a time saver!

The problem with Lineage OS though, is that it doesn't include root, which is
required by oandbackup to work. I therefore did also flash
[SuperSU](http://www.supersu.com/) onto the phone. With SuperSU on the phone,
oandbackup worked almost flawlessly.

There are a few apps that won't work correctly if they are copied (with
oandbackup). For instance, a Swedish banking app I'm using had to be
reconfigured, which I presume is due to security reasons. I also had to
configure
[DAVdroid](https://f-droid.org/repository/browse/?fdid=at.bitfire.davdroid)
again. Another app I struggled with was [Android
Auto](https://play.google.com/store/apps/details?id=com.google.android.projection.gearhead),
which although worked (and since version 2 can be run stand-alone on your phone
without connecting it to a car), but didn't display any of my media apps. For
Spotify, Audible and Pocket Casts to appear in Android Auto, I had to reinstall
those apps.

While I was upgrading to Lineage OS, I also took the opportunity to upgrade the
recovery software. I'm running [TWRP](https://twrp.me/), and upgraded it to
3.0.2.

Lineage OS, just as CyanogenMod, doesn't come with any of Google's proprietary
apps pre-installed. I have been running phones and tablets without GApps in the
past, but nowadays I prefer to have access to at least some apps from the Play
Store. I therefore flashed the pico variant of the [Open
GApps](http://opengapps.org/) binary onto the phone. However, it's buggy, at
least on Lineage OS. During the setup process of the phone, I was asked by
GApps to copy my Google settings using NFC from my previous phone. But I don't
own a second phone. Luckily, I could borrow my brother's Android phone and
trick my way through that step and later cancel the copy process (because I
sure didn't want his accounts, apps and settings on my phone).

Now I have my phone set up and I feel at home once again. It seems to be a bit
unstable though, since I have already experienced a few forced reboots.
However, CyanogenMod has always behaved in the same way on this very same
phone, so I'm not sure if I can blame Lineage OS for that.
