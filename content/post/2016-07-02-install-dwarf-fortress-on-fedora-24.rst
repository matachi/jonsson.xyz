+++
title = "Install Dwarf Fortress on Fedora 24"
date = "2016-07-02T17:00:00Z"
tags = ["Linux", "Fedora", "Dwarf Fortress", "install"]
slug = "install-dwarf-fortress-on-fedora-24"
+++

1. Download the latest version of Dwarf Fortress from
   `<http://www.bay12games.com/dwarves/>`_.

2. Untar the archive::

   $ tar -xjf df_XX_YY_linux.tar.bz2
   $ cd df_linux

3. Install the following dependencies::

   $ sudo dnf install SDL.i686 SDL_image.i686 SDL_ttf.i686 mesa-libGLU.i686 openal-soft.i686

4. Delete one of the bundled libraries::

   $ rm libs/libstdc++.so.6

5. Add a line to the startup script::

   $ sed -i '2i\export LD_PRELOAD=/usr/lib/libz.so.1' df

