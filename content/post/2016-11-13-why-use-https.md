+++
title = "Why use HTTPS? These are the advantages"
tags = ["HTTP", "HTTPS", "TLS"]
date = "2016-11-13T10:49:00+01:00"
slug = "use-https-advantages"
+++

![HTTPS](/images/https.jpg)

There are several reasons why you would want to use HTTPS for your website over
regular HTTP. They can be summarized into the following bullet points:

* **Encryption**. The data between your website's server and your users'
  browsers is exchanged in an encrypted format, keeping it secure. This makes
  it impossible for a man-in-the-middle to eavesdrop and see what you are
  sending back and forth.
* **Data Integrity**. Data cannot be modified or corrupted during transfer
  without being detected. This makes it impossible for a man-in-the-middle to
  inject malicious contents into the web requests.
* **Authentication**. Proves that your users communicate with the intended
  website.
* **SEO**. Google gives a slight ranking boost to websites that use HTTPS.
* **Performance**. The major web browsers do only support HTTP 2.0 over a HTTPS
  connection. With HTTP you are limited to HTTP 1.1. Therefore, to be able to
  take advantage of the performance improvements in HTTP 2.0, you need to use
  HTTPS.

