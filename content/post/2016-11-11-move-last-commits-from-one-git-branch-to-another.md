+++
title = "Move last commits from one Git branch to another"
tags = ["Git", "Version control system"]
date = "2016-11-11T17:13:00+01:00"
slug = "move-commits-from-one-git-branch-to-another"
+++

![Git](/images/git-logo.png)

I had made commits for a couple of different purposes on my `develop` branch.
However, the last 11 of those I later wanted to move to a second `release`
branch. Because on `develop` I have more experimental things, while on the
`release` branch more stable things that need to go out to production. These 11
commits were quite stable and desired, so letting them bake in `develop` for a
future release was not optimal.

The first solution that comes to mind is to merge `develop` into `release`.
That's the usual approach to get commits from one branch into another. However,
I still wanted to keep some older commits on `develop`, and only get my last 11
commits over to `release`.

What I ended up with was cherry-picking the 11 commits from `develop` to
`release`, and then hard resetting `develop` 11 commits back into its past. The
commands I used were:

    $ git checkout release
    $ git cherry-pick 13e2acd^..5fd9e73  # [first commit SHA]^..[last commit SHA]
    $ git push
    $ git checkout develop
    $ git reset --hard HEAD~11
    $ git push --force

The `^` character in the `cherry-pick` line is required to include the first
commit. Otherwise, it's excluded from the range.

Pushing with `--force` to `develop` is necessary since we are rewriting history
(by deleting commits). The `--force` is always needed whenever you try to push
a change that affects the already committed history in any way.

