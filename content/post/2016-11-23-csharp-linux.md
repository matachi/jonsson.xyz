+++
title = "How to compile and run C# code on Linux"
tags = [".NET", "C#", "Fedora", "Linux", "Mono"]
date = "2016-11-23T22:04:00+01:00"
slug = "csharp-linux"
+++

![Mono](/images/mono-logo.png)

To compile and run C# code on Linux, a popular open-source implementation of
the .NET framework is [Mono](https://en.wikipedia.org/wiki/Mono_(software)).
It's compatible with most Linux distributions, such as Ubuntu, Debian, Fedora,
Arch Linux, etc.

Since I'm running Fedora on my laptop, I would
[install
Mono](https://developer.fedoraproject.org/tech/languages/csharp/mono-installation.html) with the following command:

    $ sudo dnf install mono-devel

There are more installation instructions on their
[website](http://www.mono-project.com/docs/getting-started/install/linux/), but
I bet it's already packaged and available in most distributions' repositories.

Now, we will create a directory where we can do our testing:

    $ mkdir /tmp/csharp
    $ cd /tmp/csharp

Next, it's time for creating a hello world program:

~~~bash
% echo '''using System;

public class HelloWorld
{
    static public void Main()
    {
        Console.WriteLine("Hello Mono World");
    }
}''' > hello.cs
~~~

Compile and run the program with the following two commands:

    $ mcs hello.cs
    $ mono hello.exe
    Hello Mono World

Read more on:

* [http://www.mono-project.com/docs/getting-started/mono-basics/](http://www.mono-project.com/docs/getting-started/mono-basics/)

