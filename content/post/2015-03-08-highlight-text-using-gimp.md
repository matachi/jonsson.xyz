+++
title = "Highlight text using GIMP"
description = "Tutorial for how to highlight text with GIMP."
tags = ["highlight", "text", "GIMP", "tutorial"]
date = "2015-03-08"
+++

In this post I will show how you easily can highlight text in an image using
the free image editor [GIMP](http://www.gimp.org/).

## Step 1

Open an image file containing text in GIMP. An example is shown below:

![Sample text](/images/gimp/img1.png)

## Step 2

Create a new empty layer above it:

![GIMP layers](/images/gimp/img2.png)

## Step 3

Select the new layer. Then use the *paintbrush tool* with a color of your
liking and draw on the text you want to highlight:

![Painted text](/images/gimp/img3.png)

## Step 4

Change the mode of the layer containing the drawing from *Normal* to *Darken
only*:

![GIMP change layer mode](/images/gimp/img4.png)

## Done

This is the final result:

![Marked text](/images/gimp/img5.png)
