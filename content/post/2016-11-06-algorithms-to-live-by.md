+++
title = "Algorithms to live by: The computer science of human decisions"
tags = ["Algorithms to live by"]
date = "2016-11-06T12:29:00+01:00"
slug = "algorithms-live-computer-science-human-decisions"
+++

![Algorithms to live by](/images/algorithms-to-live-by.jpg)

'[Algorithms to live by: The computer science of human
decisions](http://algorithmstoliveby.com/)' is a fun and interesting book. It
covers many computer science topics such as sorting, caching, scheduling,
randomness and game theory. In covering them, it manages to relate them to
everyday scenarios.

If you have studied computer science or software engineering, I bet you are
already familiar with the majority of topics that this book covers. However,
what 'Algorithms to live by' excels at, is that is well-researched,
well-written and provides many interesting tidbits that makes computer science
applicable to real-life scenarios. Even if I were already familiar with many of
the book's topics, it provided a depth to them that I appreciate.

Even if the book isn't revolutionary in any sense, I would highly recommend it.
It's a great summary of computer science in a fun format.

