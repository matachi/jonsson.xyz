+++
title = "Install Go and Hugo on Fedora"
tags = ["Go", "Hugo", "Fedora"]
date = "2017-01-21T12:00:00+01:00"
slug = "install-go-and-hugo-on-fedora"
+++

Install the Go compiler, which is found in Fedora's own repository:

~~~sh
$ sudo dnf install go
~~~

Set `$GOPATH` in your `.bashrc` or `.zshrc`, and add its `bin` folder to your
`$PATH`:

~~~sh
$ echo "\nexport GOPATH=~/go\nexport PATH=\$PATH:~/go/bin" >> ~/.zshrc
$ source ~/.zshrc
~~~

A pre-built version of [Hugo](https://github.com/spf13/hugo) cannot be found
within Fedora's repository. So we have to build it from source instead. To
download the latest released version of Hugo from Github we will use
[gopkg.in](http://labix.org/gopkg.in), to avoid installing from master which
might not be stable:

~~~sh
$ go get -v gopkg.in/spf13/hugo.v0
~~~

If successful, it should now be possible to run Hugo:

~~~sh
$ hugo.v0 version
Hugo Static Site Generator v0.19-DEV linux/amd64 BuildDate: 2017-01-21T12:48:27+01:00
~~~

(I don't know why it says `v0.19-DEV` though, since the source code in
`~/go/src/gopkg.in/spf13/hugo.v0/` seems to correspond to 0.18.1 in my case.


If you also have ReStructuredText source files, install `python-docutils`:

~~~sh
$ sudo dnf install python-docutils
~~~
