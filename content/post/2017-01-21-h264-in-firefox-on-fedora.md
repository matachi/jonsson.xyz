+++
title = "Enable H.264 in Firefox on Fedora"
tags = ["H.264", "Firefox", "Fedora"]
date = "2017-01-21T14:00:00+01:00"
slug = "h264-in-firefox-on-fedora"
+++

On [youtube.com/html5](https://www.youtube.com/html5) it's possible to see what
video formats are supported by the browser. For me on Fedora 25, I wasn't able
to watch `H.264` nor `MSE & H.264`.

Start by installing [RPM Fusion](https://rpmfusion.org/Configuration). When
that's done, install the package `gstreamer1-plugins-ugly`:

~~~sh
sudo dnf install gstreamer1-plugins-ugly
~~~

Restart Firefox and revisit [youtube.com/html5](https://www.youtube.com/html5).
You should now have a blue checkmark on all six video formats.
