+++
title = "Shim and polyfill"
tags = ["Browser", "CSS", "HTML", "JavaScript", "Polyfill", "Shim"]
date = "2016-11-08T21:25:00+01:00"
slug = "shim-and-polyfill"
+++

![Polyfill](/images/polyfill.jpg)

A shim patches an API with functionality that isn't available in the
environment. If your code is running in an old environment, you may use a shim
to get access to newer features within the environment. However, those features
would need to be re-implemented with the building blocks provided within the
old environment.

A polyfill is a type of shim within the browser, which adds missing HTML, CSS
or JavaScript features. For instance, more recent browsers might support some
exotic JavaScript API functions that you want to use in your app, in that case
you can use a polyfill to still support older browsers. However, the
performance will probably take a hit as the polyfilled functions need to be
implemented in pure JavaScript.

In other words, shim is the more general term. While polyfill is a shim within
a browser context.

Read more on:

* [stackoverflow.com/questions/6599815/what-is-the-difference-between-a-shim-and-a-polyfill](http://stackoverflow.com/questions/6599815/what-is-the-difference-between-a-shim-and-a-polyfill)
* [blog.respoke.io/post/111278536998/javascript-shim-vs-polyfill](http://blog.respoke.io/post/111278536998/javascript-shim-vs-polyfill)
* [2ality.com/2011/12/shim-vs-polyfill.html](http://www.2ality.com/2011/12/shim-vs-polyfill.html)
* [github.com/Modernizr/Modernizr/wiki/HTML5-Cross-Browser-Polyfills](https://github.com/Modernizr/Modernizr/wiki/HTML5-Cross-Browser-Polyfills)

