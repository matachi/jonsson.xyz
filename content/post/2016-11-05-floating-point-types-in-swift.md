+++
title = "Floating-point types in Swift"
tags = ["Swift"]
date = "2016-11-05T22:15:00+01:00"
slug = "floating-point-types-in-swift"
+++

![Xcode](/images/xcode-swift-floating-point.jpg)

In Swift, there are three different floating-point types:
[Float](https://developer.apple.com/reference/swift/float),
[Double](https://developer.apple.com/reference/swift/double) and
[Float80](https://developer.apple.com/reference/swift/float80). Float
is single-precision and uses 32 bits; 8 for the exponent and 23 for the
significand. Double is double-precision and uses 64 bits; 11 for the exponent
and 52 for the significand. Float80 uses the extended precision format (using
80 bits as the name suggest); 15 for the exponent and 63 for the significand.

There are also two aliases.
[Float32](https://developer.apple.com/reference/swift/float32) is an alias for
Float, and [Float64](https://developer.apple.com/reference/swift/float64) is an
alias for Double.

These types conform to the
[FloatingPoint](https://developer.apple.com/reference/swift/floatingpoint)
protocol. It's useful when you want to make sure that the variables are any
floating-point type as in the following example:

~~~swift
func hypotenuse<T: FloatingPoint>(_ a: T, _ b: T) -> T {
    return (a * a + b * b).squareRoot()
}
hypotenuse(3, 4)  // returns 5
~~~

In this example, we use the squareRoot method declared in the FloatingPoint
protocol. The function is called with the arguments 3 and 4, which are
implicitly set to Doubles.

In Apple's Swift documentation, we also find this useful note about the
floating-point types:

> `Double` has a precision of at least 15 decimal digits, whereas the precision
> of `Float` can be as little as 6 decimal digits. The appropriate
> floating-point type to use depends on the nature and range of values you need
> to work with in your code. In situations where either type would be
> appropriate, `Double` is preferred.

Read more on:

* [en.wikipedia.org/wiki/Floating_point](https://en.wikipedia.org/wiki/Floating_point)
* [en.wikipedia.org/wiki/IEEE_floating_point](https://en.wikipedia.org/wiki/IEEE_floating_point)
* [en.wikipedia.org/wiki/Extended_precision](https://en.wikipedia.org/wiki/Extended_precision)
* [developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html)

