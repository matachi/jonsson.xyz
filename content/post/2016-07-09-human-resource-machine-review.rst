+++
title = "Human Resource Machine review"
date = "2016-07-09T12:00:00Z"
tags = ["Human Resoure Machine", "game"]
+++

.. image:: /images/human-resource-machine/screenshot.png
  :alt: Screenshot of the puzzle game Human Resource Machine

`Human Resource Machine <http://tomorrowcorporation.com/humanresourcemachine>`_
is a nice little puzzle game where you are tasked to transform blocks according
to some given instructions. Each level has a new set of instructions, which
might be to multiply each pair of numbers, or to sum each zero-terminated
sequence of numbers.

To solve these tasks, you have to program your worker with appropriate
instructions. The instruction set you have at your disposal is a small number
of assembly instructions. You are able to perform actions such as copy from and
to memory locations, add and subtract numbers, jump and conditionally jump
between instructions, and follow memory pointers.

Besides producing the level's instructed outputs, each level also has two
additional goals that you may try to achieve. One is to use a maximum number of
instructions in your solution, while the other is to make your worker take a
maximum number of actions. These goals are analogous to a compiler's
`space-time tradeoff <https://en.wikipedia.org/wiki/Space–time_tradeoff>`_.

Being a programmer who have done programming in C and assembly, I did not find
this game hard to grock. I think it was entertaining, and a fun exercise trying
to achieve some of the optional optimization goals. The complexity of the
levels was suitable, where it culminates with you having to implement a sorting
algorithm, where I opted for implementing bubble sort.

I got my copy of Human Resource Machine from `GOG
<https://www.gog.com/game/human_resource_machine>`_ for about $10. It ran fine
under Linux on my ThinkPad T440 laptop with only Intel's integrated graphics.
