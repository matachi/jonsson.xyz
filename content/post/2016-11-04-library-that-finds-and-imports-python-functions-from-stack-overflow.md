+++
title = "Library that finds and imports Python functions from Stack Overflow"
tags = ["Python"]
date = "2016-11-04T19:03:00+01:00"
slug = "library-that-finds-and-imports-python-functions-from-stack-overflow"
+++

![StackOverflow Importer](/images/stackoverflow-importer.jpg)

There's a Python library called [StackOverflow
Importer](https://github.com/drathier/stack-overflow-import) that lets you
treat Stack Overflow as a giant package from which you can import any useful
function to your Python session.

An example of what you may do in the Python REPL with this library:

~~~python
>>> from stackoverflow import quick_sort
>>> quick_sort.sort([1337, 2, 3, 1])
[1, 2, 3, 1337]
>>> from stackoverflow import file_creation_modification_date
>>> file_creation_modification_date.modification_date('/tmp/develop2.jpg')
datetime.datetime(2016, 11, 1, 22, 9, 11, 265510)
~~~

Quite fun and useful in theory. Sadly, very few of the imports I tried did
actually work in practice. This is due to many of the answers not being written
as easily imported functions.

